import { LocalStorageService } from './../../auth/service/local-storage.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { CardService } from './service/card.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bingocard',
  templateUrl: './bingoCard.component.html',
  styleUrls: ['./bingoCard.component.scss']
})
export class BingoCardComponent implements OnInit {
  card$: any = [];
  stateLoader: boolean = false;
  isLoading: boolean = true;

  constructor(
    private cardService: CardService,
    private localStorageService: LocalStorageService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.getCard();
  }

  getCard(){
    this.cardService.getCardUser()
    .subscribe(
      (card:any) => {
        
        const { data } = card.data;
        
        if(card.data.status === 401){

          Swal.fire({
            icon: 'warning',
            title: 'Sessión invalida',
            text: 'Vuelve a ingresa tus credenciales por favor.',
          })

          this.localStorageService.remove("_TK");
          this.route.navigateByUrl("login");
        }

        this.card$ = data;
        this.isLoading = false;
      }
    )
  }

}
